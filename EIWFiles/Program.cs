﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace EIWFiles
{
    class Program
    {
        static void Main(string[] args)
        {

            SqlConnection connection;
            SqlDataReader reader;

            string rootPath = @"D:\EPSEIW\Outgoing\";
            string destPath = @"D:\EPSEIW\Backup\";

            string[] files = Directory.GetDirectories(rootPath, "*.*", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                string records = files[i].Substring(19);
                
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString);
                connection.Open();
                reader = new SqlCommand($"SELECT FileFullName FROM AgentDocumentFiles WHERE FileFullName = '{records}' AND INTERFACEKEY3 IS NOT NULL ", connection).ExecuteReader();
              
                if (reader.HasRows)
                {
                    string filePath = Path.Combine(rootPath, records);
                    string backupFilePath = Path.Combine(destPath, records);

                    //Create Backup Directory
                    Directory.CreateDirectory(destPath + records);
                    Console.WriteLine($"Directory {destPath + records} Created Successfully");

                    //Get FileName to Move
                    string[] fnaList = Directory.GetFiles(filePath, "*.*", SearchOption.AllDirectories);

                    foreach(string fna in fnaList)
                    {
                        string fName = fna.Substring(26);

                        //Move processed file to backup folder
                        try
                        {
                            File.Copy(Path.Combine(filePath, fName), Path.Combine(backupFilePath, fName));
                            Console.WriteLine("File Moved Successfully");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        
                    }
                    
                }
                else
                {
                   Console.WriteLine($"{records} - Not Processed Yet"); 
                }

               
                connection.Close();
            }
          
            Console.ReadKey();
          
        } 
    } 
}

    





    


    

